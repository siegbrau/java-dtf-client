# DTF Java API client TODO 

[![Pipeline Status](https://gitlab.com/siegbrau/java-dtf-client/badges/master/pipeline.svg)](https://gitlab.com/siegbrau/java-dtf-client/-/commits/master)
[![Build Status](https://semaphoreci.com/api/v1/projects/2f1a5809-418b-4cc2-a1f4-819607579fe7/400484/shields_badge.svg)](https://semaphoreci.com/gitlabhq/gitlabhq)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=siegbrau_java-dtf-client&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=siegbrau_java-dtf-client)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=siegbrau_java-dtf-client&metric=coverage)](https://sonarcloud.io/dashboard?id=siegbrau_java-dtf-client)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=siegbrau_java-dtf-client&metric=ncloc)](https://sonarcloud.io/dashboard?id=siegbrau_java-dtf-client)

## About

This is a client for [DTF journal API](https://www.notion.so/komitet/API-TJ-vc-ru-DTF-3f5162d2cb184f6381ff82c085bbb3c0) written in Java using Jersey and Jackson. Current version is ~~`v1.8`~~ `v1.9`

## Setup 

1. Clone repository
2. Run mvn package
3. Run application
Currently are implemented 3 types of authorization:
- By personal token (recommended by DTF API) 
        Usage:`   DtfApi dtfApi = new DtfTokenApi("VALID_TOKEN");`
- By username and password.  Usage:  `DtfApi dtfApi = DtfAuthApi(String username, String password)`
- By device parameters. Usage: ` DtfApi dtfApi = DtfNoAuthApi(String appName, String appVersion, String deviceName, String osName,
                        String osVersion, String locale,
                        String osHeight, String osWidth)`

## Technology stack
- Java 11
- Jackson
- Jersey
- Slf4j

### TODO or nice to have:
- [ ] Add builder for device parameters 
- [ ] Implement authentication by QR code
- [ ] Create as maven dependency



