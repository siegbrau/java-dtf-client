package com.siegbrau.dtf.constants;

/**
 * Only for static access, do not instantiate this class.
 */
public class ApiConstants {
    public static final String BASE_DTF_URL = "https://api.dtf.ru/v1.9/";
    public static final String BASE_VC_URL = "https://api.vc.ru/v1.8/";
    public static final String BASE_TJ_URL = "https://api.tjournal.ru/v1.8/";

    public static final String RESPONSE_FIELD_OK = "ok";
    public static final String RESPONSE_FIELD_RESULT = "result";

    public static final String FILTER_HEADER_KEY = "x-filter-header";
    public static final String DEVICE_TOKEN_KEY = "X-Device-Token";
    public static final String USER_AGENT_KEY = "User-Agent";

    private ApiConstants() {
        throw new IllegalStateException("Utility class");
    }
}
