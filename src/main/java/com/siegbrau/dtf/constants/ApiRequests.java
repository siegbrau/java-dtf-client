package com.siegbrau.dtf.constants;

/**
 * Requests for api
 * Only for static access, do not instantiate this class.
 */
public class ApiRequests {
    //    For User
    public static final String GET_USER_ME = "user/me";
    public static final String GET_USER_ID = "user/{id}";
    public static final String GET_USER_ME_UPDATES = "user/me/updates/count";
    public static final String GET_USER_COMMENTS = "user/{id}/comments";
    public static final String GET_USER_ME_COMMENTS = "user/me/comments";
    public static final String GET_USER_ENTRIES = "user/{id}/entries";
    public static final String GET_USER_FAVORITES_ENTRIES = "user/{id}/favorites/entries";
    public static final String GET_USER_FAVORITES_COMMENTS = "user/{id}/favorites/comments";
    public static final String GET_USER_FAVORITES_VACANCIES = "user/{id}/favorites/vacancies";
    public static final String GET_SUBSCRIPTIONS_ME_RECOMMENDED =
            "user/me/subscriptions/recommended";
    public static final String GET_SUBSCRIPTIONS_ME = "user/me/subscriptions/subscribed";
    public static final String GET_USER_ME_TUNE_CATALOG = "user/me/tunecatalog";
    public static final String GET_IGNORED_KEYWORDS = "subsite/get-ignored-keywords";

    public static final String POST_USER_ME_UPDATES_READ_ID = "user/me/updates/read/{id}";
    public static final String POST_USER_ME_UPDATES_READ = "user/me/updates/read";
    public static final String POST_FAVORITE_ADD = "user/me/favorites";
    public static final String POST_FAVORITE_REMOVE = "user/me/favorites/remove";
    public static final String POST_TUNE_ME_CATALOG = "user/me/tunecatalog";
    public static final String POST_ME_SAVE_AVATAR = "user/me/save_avatar";
    public static final String POST_ME_SAVE_COVER = "user/me/save_cover";
    public static final String POST_USER_ME_SUBSCRIPTION = "user/me/subscribtion";
    public static final String POST_SUBSITE_IGNORE_KEYWORDS = "subsite/ignore-keywords";

    //    For Entry
    public static final String GET_ENTRY_BY_ID = "entry/{id}";
    public static final String GET_ENTRY_LOCATE = "entry/locate";
    public static final String GET_POPULAR_ENTRIES = "entry/{id}/popular";

    //  For Timeline
    public static final String GET_TIMELINE = "timeline";
    public static final String GET_TIMELINE_HASHTAG = "timeline/mainpage";
    public static final String GET_TIMELINE_NEWS = "news/default/recent";
    public static final String GET_FLASHHOLDER = "getflashholdedentry";

    //    For Comment
    //    For Vacancy
    public static final String GET_VACANCIES = "vacancies/widget";
    //    For Subsite
    public static final String GET_SUBSITE = "subsite";
    public static final String GET_SUBSITE_ME = "subsite/me";
    public static final String GET_SUBSITE_SUBSCRIBERS = "subsite/subscribers";
    public static final String GET_SUBSITE_SUBSCRIBTIONS = "subsite/subscriptions";
    public static final String GET_SUBSITE_VOTES = "subsite/votes";
    public static final String POST_SUBSITE_SET_IS_ADULT = "subsite/setisadult";
    public static final String POST_SUBSITE_UPDATE = "subsite/update";

    private ApiRequests() {
        throw new IllegalStateException("Utility class");
    }
}
