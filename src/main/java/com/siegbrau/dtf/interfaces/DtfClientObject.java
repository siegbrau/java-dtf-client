package com.siegbrau.dtf.interfaces;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface DtfClientObject extends Serializable {
}
