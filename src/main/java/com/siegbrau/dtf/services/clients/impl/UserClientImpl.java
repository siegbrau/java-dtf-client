package com.siegbrau.dtf.services.clients.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.objects.comments.Comment;
import com.siegbrau.dtf.objects.entries.Entry;
import com.siegbrau.dtf.objects.entries.Keyword;
import com.siegbrau.dtf.objects.subsites.Subsite;
import com.siegbrau.dtf.objects.users.Notification;
import com.siegbrau.dtf.objects.users.User;
import com.siegbrau.dtf.objects.vacancies.Vacancy;
import com.siegbrau.dtf.services.clients.UserClient;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_IGNORED_KEYWORDS;
import static com.siegbrau.dtf.constants.ApiRequests.GET_SUBSCRIPTIONS_ME;
import static com.siegbrau.dtf.constants.ApiRequests.GET_SUBSCRIPTIONS_ME_RECOMMENDED;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_COMMENTS;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ENTRIES;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_FAVORITES_COMMENTS;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_FAVORITES_ENTRIES;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_FAVORITES_VACANCIES;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ID;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ME;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ME_COMMENTS;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ME_TUNE_CATALOG;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ME_UPDATES;

public class UserClientImpl implements UserClient {
    private static final Logger logger = LoggerFactory.getLogger(UserClientImpl.class);

    private Client client;

    public UserClientImpl(Client client) {
        this.client = client;
    }

    @Override
    public User getMe() {
        ResultWrapper result = client.target(BASE_DTF_URL + GET_USER_ME)
                .request(MediaType.APPLICATION_JSON)
                .get(ResultWrapper.class);
        return jsonToUser(result);
    }

    @Override
    public User getUserById(Long id) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_ID.replace("{id}", id.toString()))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToUser(result);
    }

    @Override
    public List<Comment> getMyComments() {
        ResultWrapper result = client.target(BASE_DTF_URL + GET_USER_ME_COMMENTS)
                .request(MediaType.APPLICATION_JSON)
                .get(ResultWrapper.class);
        return getComments(result);
    }

    @Override
    public List<Comment> getMyFavoriteComments() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_FAVORITES_COMMENTS.replace("{id}", "me"))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getComments(result);
    }

    @Override
    public List<Comment> getUserFavoriteCommentsById(Long userId) {
        ResultWrapper result = client.target(
                BASE_DTF_URL + GET_USER_FAVORITES_COMMENTS.replace("{id}", userId.toString()))
                .request(MediaType.APPLICATION_JSON)
                .get(ResultWrapper.class);
        return getComments(result);
    }

    @Override
    public List<Comment> getUserCommentsById(Long id) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_COMMENTS.replace("{id}", id.toString()))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getComments(result);
    }

    @Override
    public List<Entry> getMyEntries() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_ENTRIES.replace("{id}", "me"))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getEntries(result);
    }

    @Override
    public List<Entry> getUserEntriesById(Long userId) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_ENTRIES.replace("{id}", userId.toString()))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getEntries(result);
    }

    @Override
    public List<Entry> getMyFavoriteEntries() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_FAVORITES_ENTRIES.replace("{id}", "me"))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getEntries(result);
    }

    @Override
    public List<Entry> getUserFavoriteEntriesById(Long userId) {
        ResultWrapper result =
                client.target(BASE_DTF_URL +
                        GET_USER_FAVORITES_ENTRIES.replace("{id}", userId.toString()))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getEntries(result);
    }

    @Override
    public List<Vacancy> getMyFavoriteVacancies() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_FAVORITES_VACANCIES.replace("{id}", "me"))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getVacancies(result);
    }

    @Override
    public List<Vacancy> getUserFavoriteVacanciesById(Long userId) {
        ResultWrapper result =
                client.target(BASE_DTF_URL +
                        GET_USER_FAVORITES_VACANCIES.replace("{id}", userId.toString()))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getVacancies(result);
    }

    @Override
    public List<Subsite> getMySubscriptions() {
        ResultWrapper result =
                client.target(BASE_DTF_URL +
                        GET_SUBSCRIPTIONS_ME)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getSubsites(result);
    }

    @Override
    public List<Subsite> getMySubscriptionsRecommended() {
        ResultWrapper result =
                client.target(BASE_DTF_URL +
                        GET_SUBSCRIPTIONS_ME_RECOMMENDED)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getSubsites(result);
    }

    @Override
    public List<Subsite> getMyTuneCatalogById(Long userId) {
        ResultWrapper result =
                client.target(BASE_DTF_URL +
                        GET_USER_ME_TUNE_CATALOG)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return getSubsites(result);
    }

    @Override
    public List<String> getMyListOfIgnoredKeywords() {
        ResultWrapper resultWrapper = client.target(BASE_DTF_URL +
                GET_IGNORED_KEYWORDS)
                .request(MediaType.APPLICATION_JSON)
                .get(ResultWrapper.class);
        return getKeywords(resultWrapper);
    }

    @Override
    public List<Notification> getMyUpdates() {
        ResultWrapper result =
                client.target(BASE_DTF_URL +
                        GET_USER_ME_UPDATES)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String notificationJson = objectMapper.writeValueAsString(result.getResult());
            Notification[] notifications =
                    objectMapper.readValue(notificationJson, Notification[].class);
            return List.of(notifications);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT TO JSON");
            return new ArrayList<>();
        }
    }

    private User jsonToUser(ResultWrapper resultWrapper) {
        if (!resultWrapper.getMessage().toString().isBlank()) {
            logger.error("Unable to get json. No object is presented");
            return new User();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String userJson = objectMapper.writeValueAsString(resultWrapper.getResult());
            return objectMapper.readValue(userJson, User.class);
        } catch (JsonProcessingException e) {
            logger.error("Unable to convert json to User.");
            //            TODO кастомные ошибки чтобы кидал
            return new User();
        }
    }

    private List<Comment> getComments(ResultWrapper result) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String commentJson = objectMapper.writeValueAsString(result.getResult());
            Comment[] comments = objectMapper.readValue(commentJson, Comment[].class);
            return List.of(comments);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT TO JSON");
            return new ArrayList<>();
        }
    }

    private List<Entry> getEntries(ResultWrapper result) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(result.getResult());
            Entry[] entries = objectMapper.readValue(entryJson, Entry[].class);
            return List.of(entries);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT TO JSON");
            return new ArrayList<>();
        }
    }

    private List<String> getKeywords(ResultWrapper result) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String entryJson = objectMapper.writeValueAsString(result.getResult());
            Keyword entries = objectMapper.readValue(entryJson, Keyword.class);
            return List.of(entries.getKeywords());
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT TO JSON");
            return new ArrayList<>();
        }
    }

    private List<Vacancy> getVacancies(ResultWrapper result) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String vacancyJson = objectMapper.writeValueAsString(result.getResult());
            Vacancy[] vacancies = objectMapper.readValue(vacancyJson, Vacancy[].class);
            return List.of(vacancies);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT TO JSON");
            return new ArrayList<>();
        }
    }

    private List<Subsite> getSubsites(ResultWrapper result) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String subsitesJson = objectMapper.writeValueAsString(result.getResult());
            Subsite[] subsites = objectMapper.readValue(subsitesJson, Subsite[].class);
            return List.of(subsites);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT TO JSON");
            return new ArrayList<>();
        }
    }
}
