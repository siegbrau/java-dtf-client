package com.siegbrau.dtf.services.clients.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.objects.vacancies.Event;
import com.siegbrau.dtf.objects.vacancies.JobOrEventFilter;
import com.siegbrau.dtf.objects.vacancies.Vacancy;
import com.siegbrau.dtf.services.clients.VacancyEventClient;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_VACANCIES;

public class VacancyEventClientImpl implements VacancyEventClient {
    private static final Logger logger = LoggerFactory.getLogger(VacancyEventClientImpl.class);

    private Client client;

    public VacancyEventClientImpl(Client client) {
        this.client = client;
    }

    @Override
    public List<Vacancy> getJobMore() {
        ResultWrapper resultWrapper = client.target(BASE_DTF_URL + GET_VACANCIES)
                                            .request(MediaType.APPLICATION_JSON)
                                            .get(ResultWrapper.class);
        return jsonToVacancyList(resultWrapper);
    }

    @Override
    public List<JobOrEventFilter> getJobFilters() {
        ResultWrapper resultWrapper = client.target(BASE_DTF_URL + GET_VACANCIES)
                                            .request(MediaType.APPLICATION_JSON)
                                            .get(ResultWrapper.class);
        return jsonToFilters(resultWrapper);
    }

    @Override
    public List<Vacancy> getVacancies() {
        ResultWrapper resultWrapper = client.target(BASE_DTF_URL + GET_VACANCIES)
                                            .request(MediaType.APPLICATION_JSON)
                                            .get(ResultWrapper.class);
        return jsonToVacancyList(resultWrapper);
    }

    @Override
    public List<JobOrEventFilter> getEventFilters() {
        ResultWrapper resultWrapper = client.target(BASE_DTF_URL + GET_VACANCIES)
                                            .request(MediaType.APPLICATION_JSON)
                                            .get(ResultWrapper.class);
        return jsonToFilters(resultWrapper);
    }

    @Override
    public List<Event> getEvents() {
        ResultWrapper resultWrapper = client.target(BASE_DTF_URL + GET_VACANCIES)
                                            .request(MediaType.APPLICATION_JSON)
                                            .get(ResultWrapper.class);
        return jsonToEvents(resultWrapper);
    }

    private List<Vacancy> jsonToVacancyList(ResultWrapper result) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(result.getResult());
            Vacancy[] vacancies = objectMapper.readValue(entryJson, Vacancy[].class);
            return List.of(vacancies);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT (VACANCY) TO JSON");
            return new ArrayList<>();
        }
    }

    private List<JobOrEventFilter> jsonToFilters(ResultWrapper resultWrapper) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(resultWrapper.getResult());
            JobOrEventFilter[] jobOrEventFilters =
                    objectMapper.readValue(entryJson, JobOrEventFilter[].class);
            return List.of(jobOrEventFilters);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT (JobOrEventFilter) TO JSON");
            return new ArrayList<>();
        }
    }

    private List<Event> jsonToEvents(ResultWrapper resultWrapper) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(resultWrapper.getResult());
            Event[] events = objectMapper.readValue(entryJson, Event[].class);
            return List.of(events);
        } catch (JsonProcessingException e) {
            logger.error("UNABLE TO CONVERT RESULT (EVENT) TO JSON");
            return new ArrayList<>();
        }
    }
}
