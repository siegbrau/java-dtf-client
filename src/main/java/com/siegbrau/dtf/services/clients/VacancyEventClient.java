package com.siegbrau.dtf.services.clients;

import com.siegbrau.dtf.objects.vacancies.Event;
import com.siegbrau.dtf.objects.vacancies.JobOrEventFilter;
import com.siegbrau.dtf.objects.vacancies.Vacancy;

import java.util.List;

public interface VacancyEventClient {
    List<Vacancy> getJobMore();

    List<JobOrEventFilter> getJobFilters();

    List<Vacancy> getVacancies();

    List<JobOrEventFilter> getEventFilters();

    List<Event> getEvents();
}
