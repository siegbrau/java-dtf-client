package com.siegbrau.dtf.services.clients;

import com.siegbrau.dtf.objects.comments.Comment;
import com.siegbrau.dtf.objects.entries.Entry;
import com.siegbrau.dtf.objects.subsites.Subsite;
import com.siegbrau.dtf.objects.users.Notification;
import com.siegbrau.dtf.objects.users.User;
import com.siegbrau.dtf.objects.vacancies.Vacancy;

import java.util.List;

public interface UserClient {

    User getMe();

    User getUserById(Long id);

    List<Comment> getMyComments();

    List<Comment> getMyFavoriteComments();

    List<Comment> getUserFavoriteCommentsById(Long userId);

    List<Comment> getUserCommentsById(Long id);

    List<Entry> getMyEntries();

    List<Entry> getUserEntriesById(Long userId);

    List<Entry> getMyFavoriteEntries();

    List<Entry> getUserFavoriteEntriesById(Long userId);

    List<Vacancy> getMyFavoriteVacancies();

    List<Vacancy> getUserFavoriteVacanciesById(Long userId);

    List<Subsite> getMySubscriptions();

    List<Subsite> getMySubscriptionsRecommended();

    List<Subsite> getMyTuneCatalogById(Long userId);

    List<String> getMyListOfIgnoredKeywords();

    List<Notification> getMyUpdates();

}
