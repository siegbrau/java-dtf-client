package com.siegbrau.dtf.services.clients.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.objects.entries.Entry;
import com.siegbrau.dtf.services.clients.TimelineClient;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_FLASHHOLDER;
import static com.siegbrau.dtf.constants.ApiRequests.GET_TIMELINE;
import static com.siegbrau.dtf.constants.ApiRequests.GET_TIMELINE_HASHTAG;
import static com.siegbrau.dtf.constants.ApiRequests.GET_TIMELINE_NEWS;

public class TimelineClientImpl implements TimelineClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimelineClientImpl.class);
    private Client client;

    public TimelineClientImpl(Client client) {
        this.client = client;
    }

    @Override
    public List<Entry> getTimeline() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_TIMELINE)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntryList(result);
    }

    @Override
    public List<Entry> getTimelineByHashtag(String hashtag) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_TIMELINE_HASHTAG)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntryList(result);
    }

    @Override
    public List<Entry> getTimelineNews() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_TIMELINE_NEWS)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntryList(result);
    }

    @Override
    public List<Entry> getFlashholder() {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_FLASHHOLDER)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntryList(result);
    }

    private List<Entry> jsonToEntryList(ResultWrapper resultWrapper) {
        if (!resultWrapper.getMessage().toString().isBlank()) {
            LOGGER.error("Unable to get json. No object is presented");
            return new ArrayList<>();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(resultWrapper.getResult());
            Entry[] entries = objectMapper.readValue(entryJson, Entry[].class);
            return List.of(entries);
        } catch (JsonProcessingException e) {
            LOGGER.error("Unable to convert json to Entry.");
            //            TODO кастомные ошибки чтобы кидал
            return new ArrayList<>();
        }
    }
}
