package com.siegbrau.dtf.services.clients.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.objects.comments.Comment;
import com.siegbrau.dtf.objects.senders.SendComment;
import com.siegbrau.dtf.services.clients.CommentClient;
import com.siegbrau.dtf.services.clients.enums.Sorting;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ENTRIES;

public class CommentClientImpl implements CommentClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommentClientImpl.class);

    private Client client;

    public CommentClientImpl(Client client) {
        this.client = client;
    }

    @Override
    public List<Comment> getEntryComments(Long entryId, Sorting sorting) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_USER_ENTRIES.replace("{id}", "me"))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsontoCommentList(result);
    }

    private List<Comment> jsontoCommentList(ResultWrapper result) {
        if (!result.getMessage().toString().isBlank()) {
            LOGGER.error("Unable to get json. No object is presented");
            return new ArrayList<>();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(result.getResult());
            Comment[] comments = objectMapper.readValue(entryJson, Comment[].class);
            return List.of(comments);
        } catch (JsonProcessingException e) {
            LOGGER.error("Unable to convert json to Entry.");
// TODO: 25.08.2021 Add custom errors
            return new ArrayList<>();
        }
    }

    @Override
    public List<Comment> getEntryCommentsLevelsGet(Long entryId, Sorting sorting) {
        return null;
    }

    @Override
    public List<Comment> getEntryCommentsThread(Long entryId, Long commentId) {
        return null;
    }

    @Override
    public boolean postLike(Long commentId) {
        return false;
    }

    @Override
    public boolean postDislike(Long commentId) {
        return false;
    }

    @Override
    public boolean resetLike(Long commentId) {
        return false;
    }

    @Override
    public boolean postCommentEdit(Long entryId, Long commentId, String id, String attachment) {
        return false;
    }

    @Override
    public boolean postCommentSend(SendComment sendComment) {
        return false;
    }

    @Override
    public boolean postCommentSaveCommentsSeenCount(int lastSeenCount) {
        return false;
    }
}
