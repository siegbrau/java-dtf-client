package com.siegbrau.dtf.services.clients.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.objects.entries.Entry;
import com.siegbrau.dtf.services.clients.EntryClient;
import com.siegbrau.dtf.services.clients.enums.Like;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.*;

public class EntryClientImpl implements EntryClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntryClientImpl.class);
    private Client client;

    public EntryClientImpl(Client client) {
        this.client = client;
    }

    @Override
    public Entry getEntryById(int id) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_ENTRY_BY_ID.replace("{id}", String.valueOf(id)))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntry(result);
    }

    @Override
    public Collection<Entry> getPopularEntries(int id) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_POPULAR_ENTRIES.replace("{id}", String.valueOf(id)))
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntryList(result);
    }

    @Override
    public Entry getEntryLocate(String url) {
        ResultWrapper result =
                client.target(BASE_DTF_URL + GET_ENTRY_LOCATE)
                        .request(MediaType.APPLICATION_JSON)
                        .get(ResultWrapper.class);
        return jsonToEntry(result);
    }

    @Override
    public boolean postLikeEntry(Long entryId) {
        changeEntryLike(entryId, Like.LIKE);
        return false;
    }

    @Override
    public boolean postDislikeEntry(Long entryId) {
        changeEntryLike(entryId, Like.DISLIKE);

        return false;
    }

    @Override
    public boolean postResetEntry(Long entryId) {
        changeEntryLike(entryId, Like.RESET);
        return false;
    }

    private void changeEntryLike(Long entryId, Like like) {
        //TODO
    }

    private List<Entry> jsonToEntryList(ResultWrapper resultWrapper) {
        if (!resultWrapper.getMessage().toString().isBlank()) {
            LOGGER.error("Unable to get json. No object is presented");
            return new ArrayList<>();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(resultWrapper.getResult());
            Entry[] entries = objectMapper.readValue(entryJson, Entry[].class);
            return List.of(entries);
        } catch (JsonProcessingException e) {
            LOGGER.error("Unable to convert json to Entry.");
            //            TODO кастомные ошибки чтобы кидал
            return new ArrayList<>();
        }
    }

    private Entry jsonToEntry(ResultWrapper result) {
        if (!result.getMessage().toString().isBlank()) {
            LOGGER.error("Unable to get json. No object is presented");
            return new Entry();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String entryJson = objectMapper.writeValueAsString(result.getResult());
            return objectMapper.readValue(entryJson, Entry.class);
        } catch (JsonProcessingException e) {
            LOGGER.error("Unable to convert json to Entry.");
            return new Entry();
        }
    }
}
