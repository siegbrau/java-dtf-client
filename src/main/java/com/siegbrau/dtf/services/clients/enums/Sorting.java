package com.siegbrau.dtf.services.clients.enums;

public enum Sorting {
    RECENT("recent"),
    HOTNESS("hotness"),
    DATE("date"),
    DAY("DAY"),
    WEEK("week"),
    MONTH("month"),
    YEAR("YEAR"),
    POPULAR("popular");

    private String sorting;

    Sorting(String sortingType) {
        this.sorting = sortingType;
    }

    public String returnType(Sorting sorting) {
        return sorting.sorting;
    }
}
