package com.siegbrau.dtf.services.clients;

import com.siegbrau.dtf.objects.comments.Comment;
import com.siegbrau.dtf.objects.senders.SendComment;
import com.siegbrau.dtf.services.clients.enums.Sorting;

import java.util.List;

public interface CommentClient {
    List<Comment> getEntryComments(Long entryId, Sorting sorting);

    List<Comment> getEntryCommentsLevelsGet(Long entryId, Sorting sorting);

    List<Comment> getEntryCommentsThread(Long entryId, Long commentId);

    boolean postLike(Long commentId);

    boolean postDislike(Long commentId);

    boolean resetLike(Long commentId);

    boolean postCommentEdit(Long entryId, Long commentId, String id, String attachment);

    boolean postCommentSend(SendComment sendComment);

    boolean postCommentSaveCommentsSeenCount(int lastSeenCount);

}
