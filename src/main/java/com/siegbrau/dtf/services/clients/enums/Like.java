package com.siegbrau.dtf.services.clients.enums;

public enum Like {
    LIKE(1),
    DISLIKE(-1),
    RESET(0);

    private int choice;

    Like(int i) {
        choice = i;
    }

    public static int returnChoiceNumber(Like like){
        return like.choice;
    }
}
