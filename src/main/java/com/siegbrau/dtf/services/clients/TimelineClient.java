package com.siegbrau.dtf.services.clients;

import com.siegbrau.dtf.objects.entries.Entry;

import java.util.List;

public interface TimelineClient {
    List<Entry> getTimeline();

    List<Entry> getTimelineByHashtag(String hashtag);

    List<Entry> getTimelineNews();

    List<Entry> getFlashholder();

}
