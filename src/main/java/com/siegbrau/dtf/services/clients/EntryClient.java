package com.siegbrau.dtf.services.clients;

import com.siegbrau.dtf.objects.entries.Entry;

import java.util.Collection;

public interface EntryClient {
    Entry getEntryById(int id);

    Collection<Entry> getPopularEntries(int id);

    Entry getEntryLocate(String url);

    boolean postLikeEntry(Long entryId);

    boolean postDislikeEntry(Long entryId);

    boolean postResetEntry(Long entryId);

}
