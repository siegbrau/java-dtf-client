package com.siegbrau.dtf.services;

import com.siegbrau.dtf.services.clients.CommentClient;
import com.siegbrau.dtf.services.clients.EntryClient;
import com.siegbrau.dtf.services.clients.TimelineClient;
import com.siegbrau.dtf.services.clients.UserClient;
import com.siegbrau.dtf.services.clients.VacancyEventClient;

public interface DtfApi {
    UserClient getUserClient();
    EntryClient getEntryClient();
    CommentClient getCommentClient();
    TimelineClient getTimelineClient();
    VacancyEventClient getVacancyEventClient();
    boolean changeToken(String token);
}

///*
// DtfClient dtfClient = new DtfClient(token);
// User user = dtfclient.getUser();
// byte []  byete = user.getPic().toByteArray();
//
//Короче, к дтф клиенту присваиваем токен.
//У дтф клиента интерфейсы который взаимодействуют с энтрисами
//                -> DtfAllStuff()
//DtfClient ->    -> DtfUserStuff()
//                -> DtfEntriesStuff(
//
//Когда вызывают гетюзер обращается к интерфейсу который должен возвращать через функицию
//типа:
//dtfClient.getEntryById(id).getAuthor().getAccountList().get(0).get
//
//User getUser(int id){
//dtfAllStuff.getId
//}
//Потом внутри них должны быть функции для конвертации в нормальные параметры. Например во время и даты
//
//
//Создай отдельный класс конвертатор?
//Или методы
// public class GetStuffer extends DtfClientSession{
//
// @Override
// getToken()
//
// onMethodCall
//
// }
//
// На будущее можно сделать в спринге что типа авторизируешь бин в мейне и можешь его автоварить, в другие классы
//
//
// */
