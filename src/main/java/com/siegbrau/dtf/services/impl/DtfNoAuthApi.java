package com.siegbrau.dtf.services.impl;

import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.services.impl.filters.UserAgentRequestFilter;
import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

import java.util.Map;
import java.util.logging.Logger;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ID;

/**
 * Authorization without using DTF account or tokens
 */
public class DtfNoAuthApi extends DtfApiImpl {

    private Map<String, String> deviceAuth;
    private Logger logger = Logger.getLogger(DtfNoAuthApi.class.getName());

    public DtfNoAuthApi(String appName, String appVersion, String deviceName, String osName,
                        String osVersion, String locale,
                        String osHeight, String osWidth) {
        this.deviceAuth =
                Map.of("appName", appName,
                        "appVersion", appVersion,
                        "deviceName", deviceName,
                        "osName", osName,
                        "osVersion", osVersion,
                        "osHeight", osHeight,
                        "osWidth", osWidth,
                        "locale", locale);
        setWebclient();
        if (checkIsValid()) {
            setClients();
        }
    }

    @Override
    protected void setWebclient() {
        UserAgentRequestFilter userAgentRequestFilter = new UserAgentRequestFilter(deviceAuth);
        client = ClientBuilder.newBuilder()
                              .register(userAgentRequestFilter)
                              .build();
    }

    private boolean checkIsValid() {
        try {
            client.target(BASE_DTF_URL + GET_USER_ID.replace("{id}","7643"))
                  .request(MediaType.APPLICATION_JSON)
                  .get(ResultWrapper.class);
            return true;
        } catch (ForbiddenException ex) {
            logger.severe("Auth is not valid");
            return false;
        }
    }
}
