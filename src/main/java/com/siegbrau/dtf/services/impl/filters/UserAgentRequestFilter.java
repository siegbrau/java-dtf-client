package com.siegbrau.dtf.services.impl.filters;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;

import java.util.Map;

import static com.siegbrau.dtf.constants.ApiConstants.USER_AGENT_KEY;

public class UserAgentRequestFilter implements ClientRequestFilter {
    private Map<String, String> deviceAuth;

    public UserAgentRequestFilter(Map<String, String> deviceAuth) {
        this.deviceAuth = deviceAuth;
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext) {
        clientRequestContext.getHeaders().add(USER_AGENT_KEY, deviceAuth);
    }
}
