package com.siegbrau.dtf.services.impl;

import com.siegbrau.dtf.services.DtfApi;

/**
 * Authorization using DTF user QR token
 * https://www.notion.so/QR-a33f9dd1931b448bb7b779d54917044e
 */
public class DtfQrTokenApi extends DtfApiImpl implements DtfApi {
    private String token;
    private String qrToken;

    public DtfQrTokenApi(String qrToken) {
        this.qrToken = qrToken;
    }

}
