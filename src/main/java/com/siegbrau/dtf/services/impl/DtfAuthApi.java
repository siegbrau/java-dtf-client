package com.siegbrau.dtf.services.impl;

import com.siegbrau.dtf.objects.ResultWrapper;
import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import java.util.logging.Logger;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ID;

/**
 * Authorization using DTF user credentials
 * https://www.baeldung.com/jersey-sse-client-request-headers
 */
public class DtfAuthApi extends DtfApiImpl {
    private final Logger logger = Logger.getLogger(DtfAuthApi.class.getName());

    private String username;
    private String password;

    public DtfAuthApi(String username, String password) {
        this.username = username;
        this.password = password;
        setWebclient();
    }

    @Override
    protected void setWebclient() {
        HttpAuthenticationFeature credentialFeature = HttpAuthenticationFeature.basicBuilder()
                                                                               .nonPreemptive()
                                                                               .credentials(
                                                                                       username,
                                                                                       password)
                                                                               .build();
        client = ClientBuilder.newClient();
        client.register(credentialFeature);
        if (checkIsValid()) {
            setClients();
        }
    }

    private boolean checkIsValid() {
        try {
            client.target(BASE_DTF_URL + GET_USER_ID.replace("{id}", "7643"))
                  .request(MediaType.APPLICATION_JSON)
                  .get(ResultWrapper.class);
            return true;
        } catch (ForbiddenException ex) {
            logger.severe("Auth is not valid");
            return false;
        }
    }
}
