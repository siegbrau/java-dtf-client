package com.siegbrau.dtf.services.impl.filters;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;

import java.io.IOException;

import static com.siegbrau.dtf.constants.ApiConstants.DEVICE_TOKEN_KEY;

public class DeviceTokenRequestFilter implements ClientRequestFilter {
    private String deviceTokenValue;

    public DeviceTokenRequestFilter(String token) {
        this.deviceTokenValue = token;
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext) throws IOException {
        clientRequestContext.getHeaders().add(DEVICE_TOKEN_KEY, deviceTokenValue);
    }
}
