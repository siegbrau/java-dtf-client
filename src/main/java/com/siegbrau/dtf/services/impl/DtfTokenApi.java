package com.siegbrau.dtf.services.impl;

import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.services.impl.filters.DeviceTokenRequestFilter;
import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

import java.util.logging.Logger;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ME;

/**
 * Authorization using DTF user token
 */
public class DtfTokenApi extends DtfApiImpl {
    private static final Logger logger = Logger.getLogger(DtfTokenApi.class.getName());

    private String token;

    public DtfTokenApi(String token) {
        changeToken(token);
    }

    @Override
    protected void setWebclient() {
        DeviceTokenRequestFilter deviceTokenRequestFilter = new DeviceTokenRequestFilter(token);
        client = ClientBuilder.newBuilder()
                              .register(deviceTokenRequestFilter)
                              .build();
    }

    private boolean tokenIsValid() {
        try {
            client.target(BASE_DTF_URL + GET_USER_ME)
                  .request(MediaType.APPLICATION_JSON)
                  .get(ResultWrapper.class);
            return true;
        } catch (ForbiddenException ex) {
            logger.severe("Token is not valid");
            return false;
        }
    }

    /*
     *  Token change == services.clients change. Public modifier to allow to change outside DTFApi
     */
    @Override
    public boolean changeToken(String token) {
        this.token = token;
        setWebclient();
        if (tokenIsValid()) {
            setClients();
            return true;
        } else {
            return false;
        }
    }

}
