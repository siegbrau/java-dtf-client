package com.siegbrau.dtf.services.impl;

import com.siegbrau.dtf.services.DtfApi;
import com.siegbrau.dtf.services.clients.CommentClient;
import com.siegbrau.dtf.services.clients.EntryClient;
import com.siegbrau.dtf.services.clients.TimelineClient;
import com.siegbrau.dtf.services.clients.UserClient;
import com.siegbrau.dtf.services.clients.VacancyEventClient;
import com.siegbrau.dtf.services.clients.impl.CommentClientImpl;
import com.siegbrau.dtf.services.clients.impl.EntryClientImpl;
import com.siegbrau.dtf.services.clients.impl.TimelineClientImpl;
import com.siegbrau.dtf.services.clients.impl.UserClientImpl;
import com.siegbrau.dtf.services.clients.impl.VacancyEventClientImpl;
import jakarta.ws.rs.client.Client;

/**
 * A main DTF API. Sets WebClient and object @DtfClientObject services.clients.
 */
public abstract class DtfApiImpl implements DtfApi {
    protected Client client;
    private UserClient userClient;
    private EntryClient entryClient;
    private TimelineClient timelineClient;
    private CommentClient commentClient;
    private VacancyEventClient vacancyEventClient;

    /*
     * Method for different types of authorizations overriding
     */
    protected void setWebclient() {

    }

    protected void setClients() {
        this.userClient = new UserClientImpl(client);
        this.entryClient = new EntryClientImpl(client);
        this.timelineClient = new TimelineClientImpl(client);
        this.commentClient = new CommentClientImpl(client);
        this.vacancyEventClient = new VacancyEventClientImpl(client);
    }

    public UserClient getUserClient() {
        return userClient;
    }

    public EntryClient getEntryClient() {
        return entryClient;
    }

    public TimelineClient getTimelineClient() {
        return timelineClient;
    }

    public CommentClient getCommentClient() {
        return commentClient;
    }

    public boolean changeToken(String token) {
        return true;
    }

    public VacancyEventClient getVacancyEventClient() {
        return vacancyEventClient;
    }
}
