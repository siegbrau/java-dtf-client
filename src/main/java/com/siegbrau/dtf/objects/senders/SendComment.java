package com.siegbrau.dtf.objects.senders;

public class SendComment {
    private Long id;

    private String text;
    private String replyTo;
    private String attachments;

    public SendComment(Long id, String text, String replyTo, String attachments) {
        this.id = id;
        this.text = text;
        this.replyTo = replyTo;
        this.attachments = attachments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }
}
