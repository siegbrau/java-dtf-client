package com.siegbrau.dtf.objects.users;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

public class SocialAccount implements DtfClientObject {
    private static final long serialVersionUID = -2154407982962008128L;
    private static final String ID_FIELD = "id";
    private static final String URL_FIELD = "url";
    private static final String TYPE_FIELD = "type";
    private static final String USERNAME_FIELD ="username" ;

    @JsonProperty(ID_FIELD)
    private Long id;
    @JsonProperty(TYPE_FIELD)
    private int type;
    @JsonProperty(USERNAME_FIELD)
    private String username;
    @JsonProperty(URL_FIELD)
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
