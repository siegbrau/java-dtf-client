package com.siegbrau.dtf.objects.users;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

public class Notification implements DtfClientObject {
    private static final long serialVersionUID = 5286935166064621169L;

    private static final String ID_FIELD = "id";
    private static final String DATE_FIELD = "date";
    private static final String DATE_RFC_FIELD = "dateRFC";
    private static final String TEXT_FIELD = "text";
    private static final String TEXT_WO_MD_FIELD = "text_wo_md";

    private static final String URL_FIELD = "url";
    private static final String TYPE_FIELD = "type";
    private static final String ICON_FIELD = "icon";

    @JsonProperty(ID_FIELD)
    private Long id;

    @JsonProperty(DATE_FIELD)
    private int date;

    @JsonProperty(DATE_RFC_FIELD)
    private String dateRFC;

    @JsonProperty(TEXT_FIELD)
    private String text;

    @JsonProperty(TEXT_WO_MD_FIELD)
    private String commentText;

    @JsonProperty(URL_FIELD)
    private String url;
    @JsonProperty(TYPE_FIELD)
    private int type;

    @JsonProperty(ICON_FIELD)
    private String icon;
}
