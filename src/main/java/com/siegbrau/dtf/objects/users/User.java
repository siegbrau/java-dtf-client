package com.siegbrau.dtf.objects.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//https://www.jsonschema2pojo.org/
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements DtfClientObject {
	private static final long serialVersionUID = 4202772857598967057L;

	private static final String ID_FIELD = "id";
	private static final String URL_FIELD = "url";
	private static final String TYPE_FIELD = "type";
	private static final String NAME_FIELD = "name";
	private static final String DESCRIPTION_FIELD = "description";
	private static final String AVATAR_URL_FIELD = "avatar_url";
	private static final String COVER_FIELD = "cover";
	private static final String IS_SUBSCRIBED_FIELD = "is_subscribed";
	private static final String IS_VERIFIED_FIELD = "is_verified";
	private static final String IS_UNSUBSCRIBABLE_FIELD = "is_unsubscribable";
	private static final String SUBSCRIBERS_COUNT_FIELD = "subscribers_count";
	private static final String COMMENTS_COUNT_FIELD = "comments_count";
	private static final String ENTRIES_COUNT_FIELD = "entries_count";
	private static final String VACANCIES_COUNT_FIELD = "vacancies_count";
	private static final String CREATED_FIELD = "created";
	private static final String CREATED_RFC_FIELD = "createdRFC";
	private static final String KARMA_FIELD = "karma";
	private static final String SOCIAL_ACCOUNTS_FIELD = "social_accounts";
	private static final String PUSH_TOPIC_FIELD = "push_topic";
	private static final String ADVANCED_ACCESS_FIELD = "advanced_access";
	private static final String COUNTERS_FIELD = "counters";
	private static final String USER_HASH_FIELD = "user_hash";
	private static final String CONTACTS_FIELD = "contacts";

	@JsonProperty(ID_FIELD)
	private Long id;
	@JsonProperty(URL_FIELD)
	private String url;
	@JsonProperty(TYPE_FIELD)
	private int type;
	@JsonProperty(NAME_FIELD)
	private String name;
	@JsonProperty(DESCRIPTION_FIELD)
	private String description;
	@JsonProperty(AVATAR_URL_FIELD)
	private String avatarUrl;
	@JsonProperty(COVER_FIELD)
	private Object cover;
	@JsonProperty(IS_SUBSCRIBED_FIELD)
	private boolean isSubscribed;
	@JsonProperty(IS_VERIFIED_FIELD)
	private boolean isVerified;
	@JsonProperty(IS_UNSUBSCRIBABLE_FIELD)
	private boolean isUnsubscribable;
	@JsonProperty(SUBSCRIBERS_COUNT_FIELD)
	private int subscribersCount;
	@JsonProperty(COMMENTS_COUNT_FIELD)
	private int commentsCount;
	@JsonProperty(ENTRIES_COUNT_FIELD)
	private int entriesCount;
	@JsonProperty(VACANCIES_COUNT_FIELD)
	private int vacanciesCount;
	@JsonProperty(CREATED_FIELD)
	private int created;
	@JsonProperty(CREATED_RFC_FIELD)
	private String createdRFC;
	@JsonProperty(KARMA_FIELD)
	private int karma;
	@JsonProperty(SOCIAL_ACCOUNTS_FIELD)
	private List<SocialAccount> socialAccounts = null;
	@JsonProperty(PUSH_TOPIC_FIELD)
	private String pushTopic;
	@JsonProperty(ADVANCED_ACCESS_FIELD)
	private Object advancedAccess;
	@JsonProperty(COUNTERS_FIELD)
	private Object counters;
	@JsonProperty(USER_HASH_FIELD)
	private String userHash;
	@JsonProperty(CONTACTS_FIELD)
	private Object contacts;

	@JsonIgnore
	private transient Map<String, Object> additionalProperties = new HashMap<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public boolean isSubscribed() {
		return isSubscribed;
	}

	public void setSubscribed(boolean subscribed) {
		isSubscribed = subscribed;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean verified) {
		isVerified = verified;
	}

	public boolean isUnsubscribable() {
		return isUnsubscribable;
	}

	public void setUnsubscribable(boolean unsubscribable) {
		isUnsubscribable = unsubscribable;
	}

	public int getSubscribersCount() {
		return subscribersCount;
	}

	public void setSubscribersCount(int subscribersCount) {
		this.subscribersCount = subscribersCount;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public int getEntriesCount() {
		return entriesCount;
	}

	public void setEntriesCount(int entriesCount) {
		this.entriesCount = entriesCount;
	}

	public int getVacanciesCount() {
		return vacanciesCount;
	}

	public void setVacanciesCount(int vacanciesCount) {
		this.vacanciesCount = vacanciesCount;
	}

	public int getCreated() {
		return created;
	}

	public void setCreated(int created) {
		this.created = created;
	}

	public String getCreatedRFC() {
		return createdRFC;
	}

	public void setCreatedRFC(String createdRFC) {
		this.createdRFC = createdRFC;
	}

	public int getKarma() {
		return karma;
	}

	public void setKarma(int karma) {
		this.karma = karma;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Object getCover() {
		return cover;
	}

	public void setCover(Object cover) {
		this.cover = cover;
	}

	public List<SocialAccount> getSocialAccounts() {
		return socialAccounts;
	}

	public void setSocialAccounts(
			List<SocialAccount> socialAccounts) {
		this.socialAccounts = socialAccounts;
	}

	public String getPushTopic() {
		return pushTopic;
	}

	public void setPushTopic(String pushTopic) {
		this.pushTopic = pushTopic;
	}

	public Object getAdvancedAccess() {
		return advancedAccess;
	}

	public void setAdvancedAccess(Object advancedAccess) {
		this.advancedAccess = advancedAccess;
	}

	public Object getCounters() {
		return counters;
	}

	public void setCounters(Object counters) {
		this.counters = counters;
	}

	public String getUserHash() {
		return userHash;
	}

	public void setUserHash(String userHash) {
		this.userHash = userHash;
	}

	public Object getContacts() {
		return contacts;
	}

	public void setContacts(Object contacts) {
		this.contacts = contacts;
	}

	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(
			Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
}
