package com.siegbrau.dtf.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  Json root wrapper for requests
 */
public class ResultWrapper {
    private static final String RESULT_FIELD = "result";
    private static final String MESSAGE_FIELD = "message";

    @JsonProperty(RESULT_FIELD)
    private Object result;

    @JsonProperty(MESSAGE_FIELD)
    private Object message;

    public Object getMessage() {
        return message;
    }

    public Object getResult() {
        return result;
    }


    public void setMessage(Object message) {
        this.message = message;
    }
}
