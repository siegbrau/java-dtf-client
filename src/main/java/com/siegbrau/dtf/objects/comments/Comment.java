package com.siegbrau.dtf.objects.comments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;
import com.siegbrau.dtf.objects.entries.Entry;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment implements DtfClientObject {
    private static final long serialVersionUID = -641548527827593849L;

    private static final String ID_FIELD = "id";
    private static final String DATE_FIELD = "date";
    private static final String DATE_RFC_FIELD = "dateRFC";
    private static final String AUTHOR_FIELD = "author";
    private static final String TEXT_FIELD = "text";
    private static final String TEXT_WO_MD_FIELD = "text_wo_md";

    private static final String ENTRY_FIELD = "entry";
    private static final String REPLY_TO_FIELD = "replyTo";
    private static final String IS_FAVORITED_FIELD = "is_favorited";
    private static final String IS_PINNED_FIELD = "is_pinned";
    private static final String IS_EDITED_FIELD = "is_edited";
    private static final String LEVEL_FIELD = "level";
    private static final String SOURCE_ID_FIELD = "source_id";

    @JsonProperty(ID_FIELD)
    private Long id;

    @JsonProperty(DATE_FIELD)
    private int date;

    @JsonProperty(DATE_RFC_FIELD)
    private String dateRFC;

    @JsonProperty(AUTHOR_FIELD)
    private Author author;

    @JsonProperty(TEXT_FIELD)
    private String text;

    @JsonProperty(TEXT_WO_MD_FIELD)
    private String textWoMd;

    @JsonProperty(ENTRY_FIELD)
    private Entry entry;

    @JsonProperty(REPLY_TO_FIELD)
    private int replyTo;

    @JsonProperty(IS_FAVORITED_FIELD)
    private boolean isFavorited;

    @JsonProperty(IS_PINNED_FIELD)
    private boolean isPinned;

    @JsonProperty(IS_EDITED_FIELD)
    private boolean isEdited;

    @JsonProperty(LEVEL_FIELD)
    private int level;

    @JsonProperty(SOURCE_ID_FIELD)
    private int sourceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getDateRFC() {
        return dateRFC;
    }

    public void setDateRFC(String dateRFC) {
        this.dateRFC = dateRFC;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextWoMd() {
        return textWoMd;
    }

    public void setTextWoMd(String textWoMd) {
        this.textWoMd = textWoMd;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public int getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(int replyTo) {
        this.replyTo = replyTo;
    }

    public boolean isFavorited() {
        return isFavorited;
    }

    public void setFavorited(boolean favorited) {
        isFavorited = favorited;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }
}
