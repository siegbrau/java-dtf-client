package com.siegbrau.dtf.objects.comments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;
import com.siegbrau.dtf.objects.users.SocialAccount;

import java.util.List;

/*
 * A model for Author
 */
@JsonIgnoreProperties(ignoreUnknown =  true)
public class Author implements DtfClientObject {
    private static final long serialVersionUID = 7881868597921115812L;

    private static final String ID_FIELD = "id";
    private static final String URL_FIELD = "url";
    private static final String NAME_FIELD = "name";
    private static final String FIRST_NAME_FIELD = "first_name";
    private static final String LAST_NAME_FIELD = "last_name";
    private static final String CREATED_FIELD = "created";
    private static final String KARMA_FIELD = "karma";
    private static final String SOCIAL_ACCOUNTS_FIELD = "social_accounts";

    @JsonProperty(ID_FIELD)
    private Long id;
    @JsonProperty(CREATED_FIELD)
    private int created;
    @JsonProperty(FIRST_NAME_FIELD)
    private String firstName;
    @JsonProperty(LAST_NAME_FIELD)
    private String lastName;
    @JsonProperty(NAME_FIELD)
    private String name;
    @JsonProperty(URL_FIELD)
    private String url;
    @JsonProperty("avatar_url")
    private String avatarUrl;
    @JsonProperty(KARMA_FIELD)
    private int karma;

    @JsonProperty(SOCIAL_ACCOUNTS_FIELD)
    private List<SocialAccount> socialAccountsList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public List<SocialAccount> getSocialAccountsList() {
        return socialAccountsList;
    }

    public void setSocialAccountsList(List<SocialAccount> socialAccountsList) {
        this.socialAccountsList = socialAccountsList;
    }
}
