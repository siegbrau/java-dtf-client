package com.siegbrau.dtf.objects.vacancies;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

public class Company implements DtfClientObject {
    private static final long serialVersionUID = 8383619119642997804L;

    private static final String ID_FIELD = "id";
    private static final String URL_FIELD = "url";
    private static final String NAME_FIELD = "name";
    private static final String IS_VERIFIED_FIELD = "is_verified";
    private static final String LOGO_FIELD = "logo";
    private static final String TYPE_FIELD ="type" ;

    @JsonProperty(ID_FIELD)
    private Long id;
    @JsonProperty(NAME_FIELD)
    private String name;
    @JsonProperty(LOGO_FIELD)
    private String logo;
    @JsonProperty(URL_FIELD)
    private String url;
    @JsonProperty(IS_VERIFIED_FIELD)
    private boolean isVerified;
    @JsonProperty(TYPE_FIELD)
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }
}
