package com.siegbrau.dtf.objects.vacancies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vacancy implements DtfClientObject {
    private static final long serialVersionUID = -23199897922023216L;

    private static final String ID_FIELD = "id";
    private static final String TITLE_FIELD = "title";
    private static final String SALARY_TO_FIELD = "salary_to";
    private static final String SALARY_FROM_FIELD = "salary_from";

    private static final String SALARY_TEXT_FIELD = "salary_text";
    private static final String AREA_FIELD = "area";
    private static final String AREA_TEXT_FIELD = "area_text";
    private static final String SCHEDULE_FIELD = "schedule";
    private static final String SCHEDULE_TEXT_FIELD = "schedule_text";
    private static final String ENTRY_ID_FIELD = "entry_id";
    private static final String CITY_ID_FIELD = "city_id";
    private static final String CITY_NAME_FIELD = "city_name";
    private static final String FAVORITES_COUNT_FIELD = "favoritesCount";
    private static final String IS_FAVORITED_FIELD = "isFavorited";
    private static final String COMPANY_FIELD = "company";

    @JsonProperty(ID_FIELD)
    private Long id;
    @JsonProperty(TITLE_FIELD)
    private String title;
    @JsonProperty(SALARY_TO_FIELD)
    private String salaryTo;
    @JsonProperty(SALARY_FROM_FIELD)
    private String salaryFrom;
    @JsonProperty(SALARY_TEXT_FIELD)
    private String salaryText;
    @JsonProperty(AREA_FIELD)
    private int area;
    @JsonProperty(AREA_TEXT_FIELD)
    private String areaText;
    @JsonProperty(SCHEDULE_FIELD)
    private int schedule;
    @JsonProperty(SCHEDULE_TEXT_FIELD)
    private String scheduleText;
    @JsonProperty(ENTRY_ID_FIELD)
    private int entryId;
    @JsonProperty(CITY_ID_FIELD)
    private int cityId;
    @JsonProperty(CITY_NAME_FIELD)
    private String cityName;
    @JsonProperty(FAVORITES_COUNT_FIELD)
    private int favoritesCount;
    @JsonProperty(IS_FAVORITED_FIELD)
    private boolean isFavorited;
    @JsonProperty(COMPANY_FIELD)
    private Company company;

    public static String getIdField() {
        return ID_FIELD;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSalaryTo() {
        return salaryTo;
    }

    public void setSalaryTo(String salaryTo) {
        this.salaryTo = salaryTo;
    }

    public String getSalaryFrom() {
        return salaryFrom;
    }

    public void setSalaryFrom(String salaryFrom) {
        this.salaryFrom = salaryFrom;
    }

    public String getSalaryText() {
        return salaryText;
    }

    public void setSalaryText(String salaryText) {
        this.salaryText = salaryText;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getAreaText() {
        return areaText;
    }

    public void setAreaText(String areaText) {
        this.areaText = areaText;
    }

    public int getSchedule() {
        return schedule;
    }

    public void setSchedule(int schedule) {
        this.schedule = schedule;
    }

    public String getScheduleText() {
        return scheduleText;
    }

    public void setScheduleText(String scheduleText) {
        this.scheduleText = scheduleText;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getFavoritesCount() {
        return favoritesCount;
    }

    public void setFavoritesCount(int favoritesCount) {
        this.favoritesCount = favoritesCount;
    }

    public boolean isFavorited() {
        return isFavorited;
    }

    public void setFavorited(boolean favorited) {
        isFavorited = favorited;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

}
