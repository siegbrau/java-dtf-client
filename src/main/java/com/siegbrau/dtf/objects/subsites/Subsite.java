package com.siegbrau.dtf.objects.subsites;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Subsite implements DtfClientObject {
    private static final long serialVersionUID = 8256347235845958544L;

    private static final String ID_FIELD = "id";
    private static final String URL_FIELD = "url";
    private static final String NAME_FIELD = "name";
    private static final String DESCRIPTION_FIELD = "description";
    private static final String AVATAR_URL_FIELD = "avatar_url";
    private static final String IS_SUBSCRIBED_FIELD = "is_subscribed";
    private static final String IS_VERIFIED_FIELD = "is_verified";
    private static final String IS_UNSUBSCRIBABLE_FIELD = "is_unsubscribable";
    private static final String SUBSCRIBERS_COUNT_FIELD = "subscribers_count";
    private static final String COMMENTS_COUNT_FIELD = "comments_count";
    private static final String ENTRIES_COUNT_FIELD = "entries_count";
    private static final String VACANCIES_COUNT_FIELD = "vacancies_count";
    private static final String CREATED_FIELD = "created";
    private static final String CREATED_RFC_FIELD = "createdRFC";
    private static final String KARMA_FIELD = "karma";

    @JsonProperty(ID_FIELD)
    private Long id;

    @JsonProperty(URL_FIELD)
    private String url;

    @JsonProperty(NAME_FIELD)
    private String name;

    @JsonProperty(DESCRIPTION_FIELD)
    private String description;

    @JsonProperty(AVATAR_URL_FIELD)
    private String avatarUrl;

    @JsonProperty(IS_SUBSCRIBED_FIELD)
    private boolean isSubscribed;

    @JsonProperty(IS_VERIFIED_FIELD)
    private boolean isVerified;

    @JsonProperty(IS_UNSUBSCRIBABLE_FIELD)
    private boolean isUnsubscribable;

    @JsonProperty(SUBSCRIBERS_COUNT_FIELD)
    private int subscribersCount;

    @JsonProperty(ENTRIES_COUNT_FIELD)
    private int entriesCount;

    @JsonProperty(VACANCIES_COUNT_FIELD)
    private int vacanciesCount;

    @JsonProperty(CREATED_FIELD)
    private int created;

    @JsonProperty(CREATED_RFC_FIELD)
    private String createdRFC;

    @JsonProperty(KARMA_FIELD)
    private int karma;

}
