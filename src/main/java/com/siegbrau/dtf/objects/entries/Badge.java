package com.siegbrau.dtf.objects.entries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Badge implements DtfClientObject {
    private static final long serialVersionUID = -5962143199532749501L;
    private static final String TYPE_FIELD = "type";
    private static final String COLOR_FIELD = "color";
    private static final String TEXT_FIELD = "text";
    private static final String BACKGROUND_FIELD = "background";
    private static final String BORDER_FIELD = "border";

    @JsonProperty(TYPE_FIELD)
    private String type;
    @JsonProperty(COLOR_FIELD)
    private String color;
    @JsonProperty(TEXT_FIELD)
    private String text;
    @JsonProperty(BACKGROUND_FIELD)
    private String background;
    @JsonProperty(BORDER_FIELD)
    private String border;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getBorder() {
        return border;
    }

    public void setBorder(String border) {
        this.border = border;
    }
}
