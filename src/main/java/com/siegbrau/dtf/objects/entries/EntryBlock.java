package com.siegbrau.dtf.objects.entries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntryBlock implements DtfClientObject {
    private static final long serialVersionUID = -8858637926276260461L;
    private static final String TYPE_FIELD = "type";
    private static final String DATA_FIELD = "data";
    private static final String COVER_FIELD = "cover";
    private static final String ANCHOR_FIELD = "anchor";
    @JsonProperty(TYPE_FIELD)
    private String type;
    @JsonProperty(DATA_FIELD)
    private Object data;
    @JsonProperty(COVER_FIELD)
    private boolean cover;
    @JsonProperty(ANCHOR_FIELD)
    private String anchor;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isCover() {
        return cover;
    }

    public void setCover(boolean cover) {
        this.cover = cover;
    }

    public String getAnchor() {
        return anchor;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }
}
