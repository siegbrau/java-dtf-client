package com.siegbrau.dtf.objects.entries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;
import com.siegbrau.dtf.objects.comments.Author;
import com.siegbrau.dtf.objects.comments.Comment;
import com.siegbrau.dtf.objects.subsites.Subsite;

import java.util.HashMap;
import java.util.List;

/**
 * a model for Entry
 * https://cmtt-ru.github.io/osnova-api/redoc.html#tag/Entry
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Entry implements DtfClientObject {
    private static final long serialVersionUID = -8006813531646747475L;

    private static final String ID_FIELD = "id";
    private static final String TITLE_FIELD = "title";
    private static final String URL_FIELD = "webviewUrl";
    private static final String ENTRY_CONTENT_FIELD = "entryContent";

    private static final String DATE_FIELD = "date";
    private static final String DATE_RFC_FIELD = "dateRFC";
    private static final String AUTHOR_FIELD = "author";
    private static final String COVER_FIELD = "cover";
    private static final String COMMENTS_COUNT_FIELD = "comments_count";
    private static final String COAUTHOR_FIELD = "co_author";
    private static final String FILLED_EDITORS_FIELD = "is_filled_by_editors";
    private static final String STILL_UPDATING_FIELD = "is_still_updating";
    private static final String SHOW_THANKS_FIELD = "is_show_thanks";
    private static final String LAST_MD_DATE_FIELD = "last_modification_date";
    private static final String TYPE_FIELD = "type";
    private static final String INTRO_FIELD = "intro";
    private static final String COMMENTS_SEEN_FIELD = "commentsSeenCount";
    private static final String REPOST_AUTHOR_FIELD = "repost";
    private static final String PROMOTED_FIELD = "is_promoted";
    private static final String REPOST_FIELD = "isRepost";
    private static final String DATE_FAV_FIELD = "date_favorite";
    private static final String CAN_EDIT_FIELD = "canEdit";
    private static final String BLOCKS_FIELD = "blocks";
    private static final String SUBSCRIBED_THREADS_FIELD = "subscribedToTreads";
    private static final String HOTNESS_FIELD = "hotness";
    private static final String SUBSITE_FIELD = "subsite";
    private static final String AVATARS_FIELD = "commentatorAvatars";
    private static final String BADGE_FIELD = "badges";
    private static final String AUDIOURL_FIELD = "audioUrl";
    private static final String PINNED_FIELD = "isPinned";
    private static final String EDITORIAL_FIELD = "isEditorial";
    private static final String ENABLED_LIKES_FIELD = "isEnabledLikes";
    private static final String ENABLED_COMMENTS_FIELD = "isEnabledComments";
    private static final String FAVORITED_FIELD = "isFavorited";
    private static final String FAVORITES_COUNT_FIELD = "favoritesCount";
    private static final String COMMENTS_PRW_FIELD = "commentsPreview";
    private static final String LIKES_FIELD = "likes";
    private static final String HITS_COUNT_FIELD = "hitsCount";
    private static final String SIMILAR_FIELD = "similar";
    private static final String INTRO_IN_FEED_FIELD = "introInFeed";

    @JsonProperty(ID_FIELD)
    private Long id;
    @JsonProperty(TITLE_FIELD)
    private String title;
    @JsonProperty(URL_FIELD)
    private String webviewUrl;
    @JsonProperty(ENTRY_CONTENT_FIELD)
    private HashMap<String, String> entryContent;
    @JsonProperty(DATE_FIELD)
    private int date;
    @JsonProperty(DATE_RFC_FIELD)
    private String dateRFC;
    @JsonProperty(LAST_MD_DATE_FIELD)
    private int lastModificationDate;
    @JsonProperty(AUTHOR_FIELD)
    private Author author;
    @JsonProperty(TYPE_FIELD)
    private int type;
    @JsonProperty(INTRO_FIELD)
    private String intro;
    @JsonProperty(COVER_FIELD)
    private Cover cover;
    @JsonProperty(INTRO_IN_FEED_FIELD)
    private int introInFeed;
    @JsonProperty(SIMILAR_FIELD)
    private List<Similar> similar;
    @JsonProperty(HITS_COUNT_FIELD)
    private int hitsCount;
    @JsonProperty(LIKES_FIELD)
    private Object likes;
    @JsonProperty(COMMENTS_PRW_FIELD)
    private List<Comment> commentsPreview;
    @JsonProperty(COMMENTS_COUNT_FIELD)
    private int commentsCount;
    @JsonProperty(FAVORITES_COUNT_FIELD)
    private int favoritesCount;
    @JsonProperty(FAVORITED_FIELD)
    private boolean isFavorited;
    @JsonProperty(ENABLED_LIKES_FIELD)
    private boolean isEnabledLikes;
    @JsonProperty(ENABLED_COMMENTS_FIELD)
    private boolean isEnabledComments;
    @JsonProperty(EDITORIAL_FIELD)
    private boolean isEditorial;
    @JsonProperty(PINNED_FIELD)
    private boolean isPinned;
    @JsonProperty(AUDIOURL_FIELD)
    private String audioUrl;
    @JsonProperty(BADGE_FIELD)
    private List<Badge> badges;
    @JsonProperty(AVATARS_FIELD)
    private List<String> commentatorsAvatars;
    @JsonProperty(SUBSITE_FIELD)
    private Subsite subsite;
    @JsonProperty(HOTNESS_FIELD)
    private double hotness;
    @JsonProperty(SUBSCRIBED_THREADS_FIELD)
    private boolean subscribedToTreads;
    @JsonProperty(BLOCKS_FIELD)
    private List<EntryBlock> blocks;
    @JsonProperty(CAN_EDIT_FIELD)
    private boolean canEdit;
    @JsonProperty(DATE_FAV_FIELD)
    private int dateFavourite;
    @JsonProperty(REPOST_FIELD)
    private boolean isRepost;
    @JsonProperty(PROMOTED_FIELD)
    private boolean isPromoted;
    @JsonProperty(REPOST_AUTHOR_FIELD)
    private Author repostAuthor;
    @JsonProperty(COMMENTS_SEEN_FIELD)
    private HashMap<String, Integer> commentsSeenCount;
    @JsonProperty(SHOW_THANKS_FIELD)
    private boolean isShowThanks;
    @JsonProperty(STILL_UPDATING_FIELD)
    private boolean isStillUpdating;
    @JsonProperty(FILLED_EDITORS_FIELD)
    private boolean isFilledByEditors;
    @JsonProperty(COAUTHOR_FIELD)
    private Subsite coAuthor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebviewUrl() {
        return webviewUrl;
    }

    public void setWebviewUrl(String webviewUrl) {
        this.webviewUrl = webviewUrl;
    }

    public HashMap<String, String> getEntryContent() {
        return entryContent;
    }

    public void setEntryContent(HashMap<String, String> entryContent) {
        this.entryContent = entryContent;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getDateRFC() {
        return dateRFC;
    }

    public void setDateRFC(String dateRFC) {
        this.dateRFC = dateRFC;
    }

    public int getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(int lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public int getIntroInFeed() {
        return introInFeed;
    }

    public void setIntroInFeed(int introInFeed) {
        this.introInFeed = introInFeed;
    }

    public List<Similar> getSimilar() {
        return similar;
    }

    public void setSimilar(List<Similar> similar) {
        this.similar = similar;
    }

    public int getHitsCount() {
        return hitsCount;
    }

    public void setHitsCount(int hitsCount) {
        this.hitsCount = hitsCount;
    }

    public Object getLikes() {
        return likes;
    }

    public void setLikes(Object likes) {
        this.likes = likes;
    }

    public List<Comment> getCommentsPreview() {
        return commentsPreview;
    }

    public void setCommentsPreview(List<Comment> commentsPreview) {
        this.commentsPreview = commentsPreview;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public int getFavoritesCount() {
        return favoritesCount;
    }

    public void setFavoritesCount(int favoritesCount) {
        this.favoritesCount = favoritesCount;
    }

    public boolean isFavorited() {
        return isFavorited;
    }

    public void setFavorited(boolean favorited) {
        isFavorited = favorited;
    }

    public boolean isEnabledLikes() {
        return isEnabledLikes;
    }

    public void setEnabledLikes(boolean enabledLikes) {
        isEnabledLikes = enabledLikes;
    }

    public boolean isEnabledComments() {
        return isEnabledComments;
    }

    public void setEnabledComments(boolean enabledComments) {
        isEnabledComments = enabledComments;
    }

    public boolean isEditorial() {
        return isEditorial;
    }

    public void setEditorial(boolean editorial) {
        isEditorial = editorial;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }

    public List<String> getCommentatorsAvatars() {
        return commentatorsAvatars;
    }

    public void setCommentatorsAvatars(List<String> commentatorsAvatars) {
        this.commentatorsAvatars = commentatorsAvatars;
    }

    public Subsite getSubsite() {
        return subsite;
    }

    public void setSubsite(Subsite subsite) {
        this.subsite = subsite;
    }

    public double getHotness() {
        return hotness;
    }

    public void setHotness(double hotness) {
        this.hotness = hotness;
    }

    public boolean isSubscribedToTreads() {
        return subscribedToTreads;
    }

    public void setSubscribedToTreads(boolean subscribedToTreads) {
        this.subscribedToTreads = subscribedToTreads;
    }

    public List<EntryBlock> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<EntryBlock> blocks) {
        this.blocks = blocks;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public int getDateFavourite() {
        return dateFavourite;
    }

    public void setDateFavourite(int dateFavourite) {
        this.dateFavourite = dateFavourite;
    }

    public boolean isRepost() {
        return isRepost;
    }

    public void setRepost(boolean repost) {
        isRepost = repost;
    }

    public boolean isPromoted() {
        return isPromoted;
    }

    public void setPromoted(boolean promoted) {
        isPromoted = promoted;
    }

    public Author getRepostAuthor() {
        return repostAuthor;
    }

    public void setRepostAuthor(Author repostAuthor) {
        this.repostAuthor = repostAuthor;
    }

    public HashMap<String, Integer> getCommentsSeenCount() {
        return commentsSeenCount;
    }

    public void setCommentsSeenCount(
            HashMap<String, Integer> commentsSeenCount) {
        this.commentsSeenCount = commentsSeenCount;
    }

    public boolean isShowThanks() {
        return isShowThanks;
    }

    public void setShowThanks(boolean showThanks) {
        isShowThanks = showThanks;
    }

    public boolean isStillUpdating() {
        return isStillUpdating;
    }

    public void setStillUpdating(boolean stillUpdating) {
        isStillUpdating = stillUpdating;
    }

    public boolean isFilledByEditors() {
        return isFilledByEditors;
    }

    public void setFilledByEditors(boolean filledByEditors) {
        isFilledByEditors = filledByEditors;
    }

    public Subsite getCoAuthor() {
        return coAuthor;
    }

    public void setCoAuthor(Subsite coAuthor) {
        this.coAuthor = coAuthor;
    }
}
