package com.siegbrau.dtf.objects.entries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cover implements DtfClientObject {
    private static final long serialVersionUID = 6588360819883995380L;

    private static final String TYPE_FIELD = "type";
    private static final String ADDITIONAL_DATA_FIELD = "additionalData";
    private static final String THUMB_URL_FIELD = "thumbnailUrl";
    private static final String URL_FIELD = "url";
    private static final String SIZE_FIELD = "size";
    private static final String SIZE_SIMPLE = "sizeSimple";

    @JsonProperty(TYPE_FIELD)
    private int type;
    @JsonProperty(ADDITIONAL_DATA_FIELD)
    private HashMap<String, Object> additionalData;
    @JsonProperty(THUMB_URL_FIELD)
    private String thumbnailUrl;
    @JsonProperty(URL_FIELD)
    private String url;
    @JsonProperty(SIZE_FIELD)
    private HashMap<String, Double> size;
    @JsonProperty(SIZE_SIMPLE)
    private String sizeSimple;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public HashMap<String, Object> getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(HashMap<String, Object> additionalData) {
        this.additionalData = additionalData;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, Double> getSize() {
        return size;
    }

    public void setSize(HashMap<String, Double> size) {
        this.size = size;
    }

    public String getSizeSimple() {
        return sizeSimple;
    }

    public void setSizeSimple(String sizeSimple) {
        this.sizeSimple = sizeSimple;
    }
}
