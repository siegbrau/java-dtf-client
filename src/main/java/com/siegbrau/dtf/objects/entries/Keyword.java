package com.siegbrau.dtf.objects.entries;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("keywords")
public class Keyword {
    @JsonProperty
    private String[] keywords;

    public String[] getKeywords() {
        return keywords;
    }

}
