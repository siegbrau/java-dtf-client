package com.siegbrau.dtf.objects.entries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siegbrau.dtf.interfaces.DtfClientObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Similar implements DtfClientObject {
    private static final long serialVersionUID = 8150514645679735631L;

    private static final String ID_FIELD = "id";
    private static final String TITLE_FIELD = "title";
    private static final String URL_FIELD = "webviewUrl";
    private static final String DATE_FIELD = "date";
    private static final String DATE_RFC_FIELD = "dateRFC";

    @JsonProperty(ID_FIELD)
    private int id;
    @JsonProperty(TITLE_FIELD)
    private int title;
    @JsonProperty(URL_FIELD)
    private int url;
    @JsonProperty(DATE_FIELD)
    private int date;
    @JsonProperty(DATE_RFC_FIELD)
    private int dateRfc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getDateRfc() {
        return dateRfc;
    }

    public void setDateRfc(int dateRfc) {
        this.dateRfc = dateRfc;
    }
}
