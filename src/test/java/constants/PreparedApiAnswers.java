package constants;

import com.siegbrau.dtf.objects.ResultWrapper;

public class PreparedApiAnswers {
    private PreparedApiAnswers() {
        throw new IllegalStateException("Utility class");
    }

    public static final ResultWrapper GET_USER_ANSWER = new ResultWrapper();
}
