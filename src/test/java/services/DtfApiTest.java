package services;

import com.siegbrau.dtf.services.DtfApi;
import com.siegbrau.dtf.services.impl.DtfNoAuthApi;
import com.siegbrau.dtf.services.impl.DtfTokenApi;
import org.junit.Assert;
import org.junit.Test;


public class DtfApiTest {

    @Test
    public void invalidTokenIsNotPermitted() {
        DtfApi dtfApi = new DtfTokenApi("invalidToken");

        Assert.assertFalse(dtfApi.changeToken("invalidToken"));
    }

    @Test
    public void invalidNoAuthIsNotPermitted() {
        DtfApi dtfApi =
                new DtfNoAuthApi("dtf-app", "2.2.0", "Pixel 2", "Android", "9", "ru", "1920",
                        "1794");
        Assert.assertNotNull(dtfApi.getUserClient().getUserById(7129L).getName());

    }

}
