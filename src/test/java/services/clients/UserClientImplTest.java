package services.clients;

import com.siegbrau.dtf.objects.ResultWrapper;
import com.siegbrau.dtf.objects.users.User;
import com.siegbrau.dtf.services.clients.impl.UserClientImpl;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.core.MediaType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashMap;
import java.util.List;

import static com.siegbrau.dtf.constants.ApiConstants.BASE_DTF_URL;
import static com.siegbrau.dtf.constants.ApiRequests.GET_USER_ME;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserClientImplTest {
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Client client;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ResultWrapper userResultWrapper = new ResultWrapper();
    private UserClientImpl userClient;

    @Before
    public void setUp() {
        userClient = new UserClientImpl(client);
    }

    @Test
    @Ignore
    public void AssertNoResultWhenErrorMessage() {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("keywords", "ignored");
        map.put("keywords", "key");
        when(userResultWrapper.getMessage()).thenReturn("");
        when(userResultWrapper.getResult()).thenReturn(map);

        when(client.target(BASE_DTF_URL + GET_USER_ME)
                   .request(MediaType.APPLICATION_JSON)
                   .get(ResultWrapper.class)).thenReturn(userResultWrapper);
        List<String> ignoredKeywords = userClient.getMyListOfIgnoredKeywords();
        Assert.assertNotNull(ignoredKeywords);
        Assert.assertEquals(map.values(), ignoredKeywords);
    }

}
